<?php
    require "validate.php";
    require "DBconnect.php";
    
    $nameErr = $emailErr = $pwdErr = $pwdcfErr = $phoneErr = $addressErr = "";
    $name = $email = $pwd = $phone = $address = $pwdcf = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["email"])) {
            $emailErr = "Email is required";
        } elseif (!filter_var(validate($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        } elseif (strlen($_POST["email"]) > 255) {
            $emailErr = "Email is too long";
        } else {
            $email = validate($_POST["email"]);
        }

        if(empty($_POST["name"])) {
            $nameErr = "Name is required";
        } elseif (strlen($_POST["name"]) < 6 || strlen($_POST["name"]) > 200) {
            $nameErr = "Name must be 6 to 200 characters";
        } else {
            $name = validate($_POST["name"]);
        }

        if(empty($_POST["pwd"])) {
            $pwdErr = "Password is required";
        } elseif (strlen($_POST["pwd"]) <6 || strlen($_POST["pwd"]) >100) {
            $pwdErr = "Password must be 6 to 100 characters";
        } else {
            $pwd = validate($_POST["pwd"]);           
        }

        if(empty($_POST["pwdcf"])) {
            $pwdcfErr = "Password Confirm is required";
        } elseif ($_POST["pwdcf"] !== $pwd) {
            $pwdcfErr = "Password confirm not match";
        } else {
            $pwdcf = validate($_POST["pwdcf"]);
            $pwd = md5($pwd);
        }

        if(empty($_POST["phone"])) {
            $phone = "";
        } elseif (strlen($_POST["phone"]) <10 || strlen($_POST["phone"]) > 20) {
            $phoneErr = "Phone must be 10 to 20 numbers";
        } else {
            $phone = validate($_POST["phone"]);
        }

        if(empty($_POST["address"])) {
            $addressErr = "Address is required";
        } else {
            $address = validate($_POST["address"]);
        }

        if ($email && $name && $pwd && $phone && $address) {
            $temp = $conn->prepare("SELECT * FROM users WHERE mail = '$email'");
            $temp->execute();
            $result = $temp->fetchAll(PDO::FETCH_ASSOC);
            if($temp->rowCount()) {
                $error = "Email has been used";
            } else {
                $sql = "INSERT INTO users (mail, name, password, phone, address)
                VALUE ('$email', '$name', '$pwd', '$phone', '$address')";
                $conn->exec($sql);
                header("Location: login.php");
            }                    
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>Sign Up</title>
</head>

<body>
    <!--NavBar--> 

    <nav class="navbar navbar-expand-sm bg-light">
        <a href="#" class="navbar-brand">MonstarLab</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="login.php">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="signup.php">Signup</a>
            </li>
        </ul>
    </nav>

    <!--Form-->

    <h1 class="text-center text-primary my-3"> SignUp Form</h1>
    
    <div class="container my-3">
        <div class="row justify-content-center">
            <div class="col-sm-6">
                <form class="border border-primary rounded p-3" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">                 
                <h6 class="text-danger text-center"> <?php echo $error ?> </h6>
                    <div class="form-group">
                        <label for="email"> Email Address </label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>">
                        <span class="text-danger"> <?php echo $emailErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="name"> Name </label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $name; ?>">
                        <span class="text-danger"> <?php echo $nameErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="pwd"> Password </label>
                        <input type="password" class="form-control" id="pwd" name="pwd">
                        <span class="text-danger"> <?php echo $pwdErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="pwd-cf"> Password Confirm</label>
                        <input type="password" class="form-control" id="pwd-cf" name="pwdcf">
                        <span class="text-danger"> <?php echo $pwdcfErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="phone"> Phone </label>
                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $phone; ?>">
                        <span class="text-danger"> <?php echo $phoneErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="address"> Address </label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $address; ?>">
                        <span class="text-danger"> <?php echo $addressErr; ?> </span>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"> Submit </button>
                    </div>                   
                </form>
            </div>         
        </div>
    </div>

</body>
</html>