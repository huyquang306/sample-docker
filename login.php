<?php

require "validate.php";
require "DBconnect.php";

session_start();

if(isset($_COOKIE['id']) && isset($_COOKIE['password'])) {
    $cookie_id = $_COOKIE['id'];
    $cookie_pwd = $_COOKIE['password'];
    $temp = $conn->prepare("SELECT * FROM users WHERE id = '$cookie_id' ");
    $temp->execute();
    $result = $temp->fetchAll(PDO::FETCH_ASSOC);
    if(count($result) === 1) {
        $account = $result[0];
        if($account['password'] === $cookie_pwd) {
            $_SESSION["email"] = $account['mail'];
            $_SESSION["password"] = $account['password'];
            $_SESSION["id"] = $account['id'];
            $_SESSION["name"] = $account['name'];
            header("Location: loginSuccess.php");
        }
        else {         
            header("Location: logout.php");
        }        
    }
} else { 
    $email = $pwd = "";
    $emailErr = $pwdErr = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["email"])) {
            $emailErr = "Email is required";
        } elseif (!filter_var(validate($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        } elseif (strlen($_POST["email"]) > 255) {
            $emailErr = "Email is too long";
        } else {
            $email = validate($_POST["email"]);
        }
    
        if(empty($_POST["pwd"])) {
            $pwdErr = "Password is required";
        } elseif (strlen($_POST["pwd"]) <6 || strlen($_POST["pwd"]) >100) {
            $pwdErr = "Password must be 6 to 100 characters";
        } else {
            $pwd = validate($_POST["pwd"]);
            $pwd = md5($pwd);
        }
    
        if($email && $pwd){ 
            $temp = $conn->prepare("SELECT * FROM users WHERE mail = '$email' AND password = '$pwd'");
            $temp->execute();
            $result = $temp->fetchAll(PDO::FETCH_ASSOC);
            if(count($result) === 1) {
                $account = $result[0];
                $_SESSION["email"] = $account['mail'];
                $_SESSION["password"] = $account['password'];
                $_SESSION["id"] = $account['id'];
                $_SESSION["name"] = $account['name'];
                if(isset($_POST['remember-me'])) {
                    $day = 30;
                    $id = $account['id'];
                    $password = $account['password'];
                    setcookie("id", $id, time() + ($day*24*60*60*1000),"/");
                    setcookie("password", $password, time() + ($day*24*60*60*1000),"/");
                }
                header("Location: loginSuccess.php");
            } else {
                $error = "Incorrect email or password";
            } 
        }
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>Login</title>
</head>
<body>
    <!--NavBar--> 

    <nav class="navbar navbar-expand-sm bg-light">
        <a href="#" class="navbar-brand">MonstarLab</a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="login.php">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="signup.php">Signup</a>
            </li>
        </ul>
    </nav>

    <!--Form--> 
    
    <h1 class="text-center text-primary my-3"> Login Form </h1>
    <div class="container my-3">
        <div class="row justify-content-center">
            <div class="col-sm-6">
                <form class="border border-primary rounded p-3" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <h6 class="text-danger text-center"> <?php echo $error ?> </h6>
                    <div class="form-group">
                        <label for="email"> Email </label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>">
                        <span class="text-danger"> <?php echo $emailErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="pwd"> Password </label>
                        <input type="password" class="form-control" id="pwd" name="pwd">
                        <span class="text-danger"> <?php echo $pwdErr; ?> </span>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="remember" name="remember-me">
                        <label for="remember-me"> Remember me? </label>
                    </div>
                    <div class="text-center">
                        <button type="sunmit" class="btn btn-primary"> Login </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>